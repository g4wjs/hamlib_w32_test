#-------------------------------------------------
#
# Project created by QtCreator 2016-08-05T16:29:27
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = hamlib_win32_test
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h

FORMS    += mainwindow.ui \
    rig/freqdisplay.ui





INCLUDEPATH += D:/hamlib-3.0.1/include




win32: LIBS += -LD:/hamlib-w32-3.0.1/lib/gcc/ -llibhamlib



